## Influencia de imágenes sobre la eficiencia decontenedores Docker

### Estructura de carpetas

Hay 3 carpeetas cada una con su experimento, dentro de ellas hay:
- images: imagenes/graficos sacados a partid de los datos de res.xls
- tests : carpetas con la salida estandard de los tests realizados
- res.gnumeric: Excel con los datos de las pruebas (y estadisticas sobre estos)
- run.sh: Lanzado de contenedores
- test_*.sh : Test (*) sobre el contenedor
  
Ademas hay una carpeta volume, que se usara como directorio compartido entre el contenedor y host.

Se realizan tres experimentos.

### Experimento1
Consiste en comprobar si existe diferencia entre escribir a una capa base de docker y escribir a un directorio compartido con el host.

### Experimento2
Consiste en comprobar si hay alguna diferencia entre escribir a diferentes capas de la imagen creada por un gestor de ficheros de Docker. En este caso se usa Overlay2


### Experimento3
Consiste en comprobar si hay diferencias significativas entre el uso de un gestor de ficheros u otro. Los dos probados son Aufs y Overlay2.
