#!/bin/bash

for i in {11..20}
do
    echo WRITTING
    echo WRITE > ./tests/test$i
    #write
    docker exec -it  test bash /home/volume/write.sh /home/volume/rand >> ./tests/test$i

    echo READING
    echo >> ./tests/test$i
    echo READ CACHE >> ./tests/test$i
    #read buffer cache
    docker exec -it  test bash /home/volume/read.sh /home/volume/rand >> ./tests/test$i

    echo READING WITHOUT CACHE
    echo >> ./tests/test$i
    echo READ NO CACHE >> ./tests/test$i
    sudo /sbin/sysctl -w vm.drop_caches=3
    docker exec -it  test bash /home/volume/read.sh /home/volume/rand >> ./tests/test$i
done