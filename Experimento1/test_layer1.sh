#!/bin/bash

dir=./tests/layer1/test

for i in {1..20}
do
    echo WRITTING
    echo WRITE > $dir$i
    #write
    docker exec -it  test bash /home/volume/write.sh /home/layer-1/rand >> $dir$i

    echo READING
    echo >> $dir$i
    echo READ CACHE >> $dir$i
    #read buffer cache
    docker exec -it  test bash /home/volume/read.sh /home/layer-1/rand >> $dir$i

    echo READING WITHOUT CACHE
    echo >> $dir$i
    echo READ NO CACHE >> $dir$i
    sudo /sbin/sysctl -w vm.drop_caches=3
    docker exec -it  test bash /home/volume/read.sh /home/layer-1/rand >> $dir$i
done